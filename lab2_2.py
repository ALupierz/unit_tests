
    def __sub__(self, other):
        return Complex(self.real - other.real, self.imag - other.imag)
    def __mul__(self, other):
        return Complex(self.real*other.real - self.imag*other.imag, self.imag*other.real + self.real*other.imag)
  
